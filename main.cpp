#include <iostream>


/*CRTP(Curiously recurring template pattern)
странно повторяющийся шаблон шаблона
Целью этого является использование производного класса в базовом классе.
*/

template<typename T>
class Base{
    public:
     void implementation(){
    static_cast<T*>(this)->implementation();
  }




};
class Derived1:public Base<Derived1>{
    public:
    void implementation(){
        std::cout << "Implementation Derived1" << std::endl;
    }

};

class Derived2:public Base<Derived2>{
    public:
        void implementation(){
        std::cout << "Implementation Derived2" << std::endl;
    }

    
};




int main()
{
    Base<Derived1> b1;
    b1.implementation();
    Base<Derived2> b2;
    b2.implementation();

   


return 0;
 
}

